import {
  StackNavigator
} from 'react-navigation';
import {
  Platform,
  StatusBar
} from 'react-native';

import FirstScreen from '../Pages/onboarding/index';
import Login from '../Pages/login';
import Register from '../Pages/register';
import Dashboard from '../Pages/dashboard';
import EventDetail from '../Pages/eventDetail';
import TaskDetail from '../Pages/taskDetail';
import GalleryDetail from '../Pages/galleryDetail';
import RewardDetail from '../Pages/rewardDetail';
import FinalTaskCompleted from '../Pages/thankYou';
import ProfileDetail from '../Pages/profileDetail';

// main stack
const MainStack = StackNavigator({
  FirstScreen: {
    screen: FirstScreen
  },
  Login: {
    screen: Login
  },
  Register: {
    screen: Register
  },
  Dashboard: {
    screen: Dashboard
  },
  EventDetail: {
    screen: EventDetail
  },
  TaskDetail: {
    screen: TaskDetail
  },
  GalleryDetail: {
    screen: GalleryDetail
  },
  RewardDetail: {
    screen: RewardDetail
  },
  FinalTaskCompleted: {
    screen: FinalTaskCompleted
  },
  ProfileDetail: {
    screen: ProfileDetail
  },
}, {
  headerMode: 'none',
  navigationOptions: {
    gesturesEnabled: false,
  },
  style: {
    paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
  },
}, );

const PrimaryNav = StackNavigator({
  mainStack: {
    screen: MainStack
  },
}, {
  headerMode: 'none',
  initialRouteName: 'mainStack',
  gesturesEnabled: false,
}, );

export default PrimaryNav;