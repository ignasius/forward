import { Platform, StyleSheet } from 'react-native';
import { Fonts, Metrics, Colors } from '../../Themes/';

const styles = StyleSheet.create({
  imgContainer: {
    width: Metrics.WIDTH,
  },
  sectionHeader: {
    color: Colors.sectionHeader,
    color: Colors.forwardDarkBlue,
    fontWeight: 'bold',
    fontSize: 24,
  },
  cardFooterContainer: {
    flex: 1, 
    flexDirection: 'column'
  },
  sectionSeparator: {
    backgroundColor: Colors.sectionSeparator,
    marginTop: 16,
    // marginBottom: 16,
    height: 2
  },
  sectionBlue: {
    color: Colors.forwardDarkBlue,
    fontSize: 14,
  },
  sponsorsImages:{
    width:(Metrics.WIDTH*0.98),
    height:(Metrics.HEIGHT*0.15)
  }
});

export default styles;