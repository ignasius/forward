import React, { Component } from "react";
import {
  View,
  StatusBar,
  Platform,
  ImageBackground,
  SectionList,
  AsyncStorage,
} from "react-native";
import { Container } from "native-base";
import { LinearGradient } from "react-native-linear-gradient";
import { Images, Fonts, Metrics, Colors } from "../../Themes/";
import Styles from "./HomeStyle";
import GreetingView from "../home/GreetingView"
import CalendarView from "../home/CalendarView"
import EventView from "../home/EventView"
import SponsorView from "../home/SponsorView"
import { DashboardService } from '../../libs/api';
import { ProfileStorage } from '../../libs/storage';

export default class Home extends Component {
  _defaultContent = [];
  _defaultGreetingDescription = 'Welcome and get ready to start your journey with ' + 
    'Jesus! Thank you for taking your first step forward at ' + 
    'LOJ Conference, and to continue your journey we ' + 
    'have designed these 30 days challenge for you, so you ' + 
    'can stay forward and reach your life goals! Let’s start ' +
    'and get connected!';
  
  _renderSectionHeader = ({item, index, section: {title}}) => (
    <Text style={Styles.sectionHeader}>{title}</Text>
  )

  _renderGreeting = ({item, index, section: {title, data}}) => 
    <GreetingView item={item} />
    
  _renderCalendar = ({item, index, section: {title, data}}) => 
    <CalendarView item={item} navigation={this.props.navigation} navigateToForward={this.props.navigateToForward}/>

  _renderEvent = ({item, index}) => (
     <EventView item={item} navigation={this.props.navigation} />
  );

  _onEventSelected = (index) => {
    console.log("Pressed row: "+index);
  };

  _renderPoweredBy = ({item, index}) => (
     <SponsorView item={item} />
  );

  constructor(props) {
    super(props);

    this._populateDefaultState();
    this._loadContent();
  }

  _populateDefaultState = async () => {
    _defaultContent = [
        { key: 'greeting', data: [{ title: 'Hello, ', description: this._defaultGreetingDescription, quote: ''}], renderItem: this._renderGreeting },
        { key: 'progress', data: [{ title: 'Here is your 30 days challenges progress', data: ['item2'], totalDayCompleted: 0}], renderItem: this._renderCalendar },
        { key: 'events', data: [{ title: 'Upcoming Events', data: [{id: 1, date: '12 Aug 2018', title: 'Some title goes here 1', color: Colors.red},]}], renderItem: this._renderEvent },
        { key: 'backer', data: [{ title: 'Sponsored By', data: [{title: '', image: ''}, ]}], renderItem: this._renderPoweredBy },
      ];

    this.state = {
      index: "",
      content: _defaultContent
    };
  }

  _loadContent = async () => {
    DashboardService
      .dashboard()
      .then(async (response) => {
        let user = await ProfileStorage.getLocalProfile();
        let events = this._parseEventsFromResponse(response);
        let sponsors = this._parseBackersFromResponse(response);

        let newContent = [
          { key: 'greeting', data: [{ title: 'Hello, ' + user.name, description: this._defaultGreetingDescription, quote: ''}], renderItem: this._renderGreeting },          
          { key: 'progress', data: [{ title: 'Here is your 30 days challenges progress', data: ['item2'], totalDayCompleted: response.completed}], renderItem: this._renderCalendar },
          { key: 'events', data: [{ title: 'Upcoming Events', data: events}], renderItem: this._renderEvent },
          { key: 'backer', data: [{ title: 'Sponsored By', data: sponsors}], renderItem: this._renderPoweredBy },
        ];

        this.setState({
          index: "",
          content: newContent
        });
      }).catch((error) => {
          alert("Fetching dashboard failed: " + error.response.data.msg);
          console.log(error);
      });
  }

  _parseEventsFromResponse = (response) => {
    let events = [];

    for (var i = 0; i < response.events.length; i++) {
      let event = response.events[i];

      events.push({
        id: event.id,
        title: event.title,
        place: event.place,
        time: event.time,
        date: event.eventDatePreformated,
        color: i % 2 == 0 ? Colors.turquoiseBlue : Colors.purpleHeart
      });
    }

    return events;
  }

  _parseBackersFromResponse = (response) => {
    let sponsors = [];
    let baseUrl = 'https://api.lojconference.id/static';

    for (var i = 0; i < 6; i++) {
      let sponsor = response.sponsors[i];

      if (sponsor) {
        sponsors.push({
          id: sponsor.id,
          title: sponsor.title,
          image: baseUrl + sponsor.image
        });
      } else {
        sponsors.push({
          id: 0,
          title: '',
          image: ''
        });
      }
    }

    return sponsors;
  }

  render() {
    const imageUri = Images.background_main;

    StatusBar.setBarStyle("light-content", true);

    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("transparent", true);
      StatusBar.setTranslucent(true);
    }

    return (
      <Container>
        <View>
          <SectionList 
            keyExtractor={(item) => item.key} 
            sections={this.state.content}
            extraData={this.state}
          />
        </View>
      </Container>
    );
  }
}
