import React, { Component } from "react";
import {
  View,
  StatusBar,
  Platform,
  ImageBackground,
  SectionList
} from "react-native";
import { Container } from "native-base";
import { LinearGradient } from "react-native-linear-gradient";
import { Images, Fonts, Metrics, Colors } from "../../Themes/";
import Styles from "./GiftStyle";
import RewardView from "../reward/RewardView"
import { RewardService } from '../../libs/api';
import { ProfileStorage } from '../../libs/storage';

export default class Gift extends Component {
  _defaultContent = [];

  _renderNewRewards = ({item, index}) => (
     <RewardView item={item} navigation={this.props.navigation} refresh={this._loadContent} />
  );

  _renderTakenRewards = ({item, index}) => (
     <RewardView item={item} disabled='true' />
  );

  constructor(props) {
    super(props);

    this._populateDefaultState();
    this._loadContent();
  }

  _populateDefaultState = async () => {
    _defaultContent = [
                { 
                  key: 'rewards', 
                  data: [{
                    title: 'You have new rewards!', 
                    data: [
                        {id: -1, date: '', title: '', color: Colors.turquoiseBlue}, 
                        {id: -2, date: '', title: '', color: Colors.purpleHeart},
                      ]
                    }], 
                  renderItem: this._renderNewRewards 
                }, 
                { 
                  key: 'claimeds',
                  data: [{
                    title: 'Redeemed rewards', 
                    data: [
                        {id: -1, date: '', title: '', color: Colors.gray},
                        {id: -2, date: '', title: '', color: Colors.gray},
                      ]
                    }], 
                  renderItem: this._renderTakenRewards },
            ];

    this.state = {
      index: "",
      content: _defaultContent
    };
  }

  _loadContent = async () => {
    let that = this;

    RewardService
      .getRewards()
      .then(async (response) => {
        let rewards = this._parseRewardsFromResponse(response.rewards, false);
        let claimeds = this._parseRewardsFromResponse(response.claimeds, true);

        let newContent = [
          { key: 'rewards', data: [{ title: 'You have new rewards!', data: rewards}], renderItem: this._renderNewRewards },
          { key: 'claimeds', data: [{ title: 'Redeemed rewards', data: claimeds}], renderItem: this._renderTakenRewards },
        ];

        that.setState({
          index: "",
          content: newContent
        });
      }).catch((error) => {
        let data = error.response.data;
        let message = data.msg ? data.msg : '';

        alert("Fetching rewards failed: " + message);
        console.log(error);
      });
  }

  _parseRewardsFromResponse = (rewards, isTaken) => {
    let result = [];

    for (var i = 0; i < rewards.length; i++) {
      let item = rewards[i];

      result.push({
        id: item.id,
        title: item.title,
        description: item.description,
        date: item.createdAtPreformated,
        color: isTaken ? Colors.gray : (i % 2 == 0 ? Colors.turquoiseBlue : Colors.purpleHeart)
      });
    }

    return result;
  }

  render() {
    const imageUri = Images.background_main;
    
    StatusBar.setBarStyle("light-content", true);

    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("transparent", true);
      StatusBar.setTranslucent(true);
    }

    return (
      <Container>
        <View>
          <SectionList 
            keyExtractor={(item) => item.key} 
            sections={this.state.content}
            extraData={this.state}
          />
        </View>
      </Container>
    );
  }
}
