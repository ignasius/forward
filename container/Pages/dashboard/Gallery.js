import React, { Component } from "react";
import {
  Platform,
  StatusBar,
  View,
  ImageBackground,
  FlatList,
} from "react-native";
import { Container } from "native-base";
import { LinearGradient } from "react-native-linear-gradient";
import { Images, Fonts, Metrics, Colors } from "../../Themes/";
import Styles from './GalleryStyle';
import GalleryListItem from "../gallery/GalleryListItem"
import { GalleryService } from '../../libs/api';
import { ProfileStorage } from '../../libs/storage';

export default class Gallery extends Component {
  constructor(props) {
    super(props);

    this._populateDefaultState();
    this._loadContent();
  }

  _populateDefaultState = async () => {
    this.state = {
      index: "",
      loading: false,
      refreshing: false,
      content: {
        galleries: []
      }
    };
  }

  _loadContent = async () => {
    GalleryService
      .getGalleries()
      .then(async (response) => {
        let newState = Object.assign({}, this.state);

        newState.content.galleries = this._parseGalleryFromResponse(response);

        this.setState(newState);
      }).catch((error) => {
        let data = error.response.data;
        let message = data.msg ? data.msg : '';

        alert("Fetching gallery failed: " + message);
        console.log(error.response);
      });
  }

  _parseGalleryFromResponse = (response) => {
    let result = [];
    let baseUrl = 'https://api.lojconference.id/static';

    for (var i = 0; i < response.datas.length; i++) {
      let item = response.datas[i];

      result.push({
        id: item.id,
        title: item.title,
        description: item.description,
        shortDescription: item.shortDescription,
        date: item.galleryDatePreformated,
        type: item.type.toLowerCase(),
        thumbnail: baseUrl + item.thumbnail,
        youtubeUrl: item.youtubeUrl,
        images: [
          this._parseImagePath(baseUrl, item.image1), 
          this._parseImagePath(baseUrl, item.image2), 
          this._parseImagePath(baseUrl, item.image3), 
        ]
      });
    }

    return result;
  }

  _parseImagePath = (baseUrl, image) => {
    if (image) {
      return baseUrl + image;
    }

    return null;
  }

  _keyExtractor = (item, index) => index

  _renderItem = ({ item }) => (
    <GalleryListItem
      item={item}
      onPressItem={this._onEventSelected}
    />
  )

  _onEventSelected = (index) => {
    let content = this.state.content.galleries[index-1];

    this.props.navigation.navigate('GalleryDetail', content);
  };

  _renderSeparator = () => (
    <View
      style={Styles.sectionSeparator} 
    />
  );

  render() {
    const imageUri = Images.background_main;

    StatusBar.setBarStyle("light-content", true);

    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("transparent", true);
      StatusBar.setTranslucent(true);
    }

    return (
      <Container>
        <View>
            <FlatList
              keyExtractor={this._keyExtractor}
              data={this.state.content.galleries}
              extraData={this.state.content.galleries}
              renderItem={this._renderItem}
              ItemSeparatorComponent={this._renderSeparator}
              style={Styles.galleryList}
            />
        </View>
      </Container>
      
    );
  }
}
