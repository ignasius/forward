
import React, { Component } from 'react';
import { 
  Text, 
  Image, 
  StatusBar, 
  Platform, 
  ImageBackground,
  Dimensions,
  TouchableOpacity, 
  ListView,
  View,
  BackHandler, 
  I18nManager
} from 'react-native';
import { 
  Container, 
  Content, 
  Header, 
  Body, 
  Footer,
  FooterTab,
  Icon, 
  Right, 
  Left, 
  Item, 
  Input, 
  Button, 
  Title,  
  ListItem, 
} from 'native-base';
import Swiper from 'react-native-swiper';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Colors, Images, Metrics } from '../../Themes';

import Forward from './Forward.js'
import Home from './Home.js';
import Gallery from './Gallery.js';
import Gift from './Gift.js';
import Setting from './Setting.js';
import Default from './Default.js';

// Screen Styles
import styles from './styles';

export default class Dashboard extends Component {

  constructor(props) {
 		super(props);

    this.state = {
      selectedTab: "Home",
      selectedTabTitle: "Stay Forward"
    };
 	}

  renderSelectedTab () {
    switch (this.state.selectedTab) {
      case 'Forward':
        return (<Forward navigation={this.props.navigation}/>);
        break;
      case 'Gallery':
        return (<Gallery navigation={this.props.navigation}/>);
        break;
      case 'Home':
        return (<Home navigation={this.props.navigation} navigateToForward={this._navigateToForward}/>);
        break;
      case 'Gift':
        return (<Gift navigation={this.props.navigation}/>);
        break;
      case 'Setting':
        return (<Setting navigation={this.props.navigation}/>);
        break;
      default:
        return (<Default/>);
        break;
    }
  }

  _navigateToForward = () => {
    this.setState({selectedTab: 'Forward'});
  }

  render(){
		StatusBar.setBarStyle('light-content', true);

		if (Platform.OS === 'android') {
			StatusBar.setBackgroundColor("transparent", true);
      StatusBar.setTranslucent(true);
		}
    
    return(
      <Container style={styles.main}>
      <Header
          androidStatusBarColor={Colors.transparent}
          style={styles.header}>

          <Left style={styles.left}>
            <Text style={styles.headerTxt}>{this.state.selectedTabTitle.toUpperCase()}</Text>
          </Left>

          <Body style={styles.body}>
            
          </Body>

          <Right style={styles.right}>
            <Button transparent>
            </Button>
          </Right>
      </Header>
      <View style={styles.main}>
        {this.renderSelectedTab()}
      </View>
        <FooterTab style={styles.footerTabBg}>
          <Button vertical
            active={this.setState.selectedTab==='Home'}
            onPress={() => this.setState({selectedTab: 'Home', selectedTabTitle: `Stay Forward`})}>
            {
              (this.state.selectedTab == 'Home') ?
              <View style={{justifyContent: 'center'}}>
                <MaterialCommunityIcons name="home" size={22} color="#70dfe8"/>
              </View>
              :
              <View style={{justifyContent: 'center'}}>
                <MaterialCommunityIcons name="home" size={22} color="#929292"/>
              </View>
            }
            <Text style={styles.footerTitle}>Home</Text>
          </Button>
          <Button vertical
            onPress={() => this.setState({selectedTab: 'Forward', selectedTabTitle: 'Forward'})}>
            {
              (this.state.selectedTab == 'Forward') ?
              <View style={{justifyContent: 'center'}}>
                <MaterialCommunityIcons name="fast-forward" size={22} color="#70dfe8"/>
              </View>
              :
              <View style={{justifyContent: 'center'}}>
                <MaterialCommunityIcons name="fast-forward" size={22} color="#929292"/>
              </View>
            }
            <Text style={[{marginTop: 5},styles.footerTitle]}>Forward</Text>
          </Button>
          <Button vertical
            onPress={() => this.setState({selectedTab: 'Gallery', selectedTabTitle: 'Highlights'})}>
            {
              (this.state.selectedTab == 'Gallery') ?
              <View style={{justifyContent: 'center'}}>
                <MaterialCommunityIcons name="image" size={22} color="#70dfe8"/>
              </View>
              :
              <View style={{justifyContent: 'center'}}>
                <MaterialCommunityIcons name="image" size={22} color="#929292"/>
              </View>
            }
            <Text style={[{marginTop: 3},styles.footerTitle]}>Highlights</Text>
          </Button>
          <Button vertical
            onPress={() => this.setState({selectedTab: 'Gift', selectedTabTitle: 'Rewards'})}>
            {
              (this.state.selectedTab == 'Gift') ?
              <View style={{justifyContent: 'center'}}>
                <MaterialCommunityIcons name="gift" size={22} color="#70dfe8"/>
              </View>
              :
              <View style={{justifyContent: 'center'}}>
                <MaterialCommunityIcons name="gift" size={22} color="#929292"/>
              </View>
            }
            <Text style={[{marginTop: 3},styles.footerTitle]}>Rewards</Text>
          </Button>
          <Button vertical
            onPress={() => this.setState({selectedTab: 'Setting', selectedTabTitle: 'Setting'})}>
            {
              (this.state.selectedTab == 'Setting') ?
              <View style={{justifyContent: 'center'}}>
                <EvilIcons name="gear" size={28} color="#70dfe8"/>
              </View>
              :
              <View style={{justifyContent: 'center'}}>
                <EvilIcons name="gear" size={28} color="#929292"/>
              </View>
            }
            <Text style={[{marginTop: 3},styles.footerTitle]}>Setting</Text>
          </Button>
        </FooterTab>
      </Container>
    );
  }
}
