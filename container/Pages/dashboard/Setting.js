import React, { Component } from "react";
import {
  Platform,
  StatusBar,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  ListView,
  Dimensions,
} from "react-native";
import {
  Container,
  Card, 
  CardItem, 
  Title,
  Header,
  Body,
  Icon,
  Right,
  Left,
  Button,
  Text, 
} from "native-base";
import LinearGradient from "react-native-linear-gradient";
import { Images, Fonts, Metrics, Colors } from "../../Themes/";
import Styles from "./SettingStyle";
import { TokenStorage, ProfileStorage } from '../../libs/storage';

export default class Setting extends Component {
  constructor(props) {
    super(props);

    this.state = {
      index: ""
    };
  }

  _navigateToChangeProfile = () => {
    this.props.navigation.navigate('ProfileDetail');
  }

  _logout = () => {
    if (this._localStorageRemoved()) {
      this.props.navigation.navigate('Login');
    }
  }

  _localStorageRemoved = () => {
    const tokenRemoved = TokenStorage.removeToken();
    const profileRemoved = ProfileStorage.removeProfile();

    return tokenRemoved && profileRemoved;
  }

  render() {
    const imageUri = Images.background_main;

    StatusBar.setBarStyle("light-content", true);

    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("transparent", true);
      StatusBar.setTranslucent(true);
    }

    return (
      <Container>
        <Card transparent>
          <CardItem header>
          </CardItem>
          <CardItem>
            <Body>
              <Text style={Styles.description}>
                Personal Setting
              </Text>
            </Body>
          </CardItem>
          <CardItem>
            <Body style={{
                flex: 1,
                flexDirection: 'column',
              }}>
              
              <TouchableOpacity style={Styles.buttonSignIn} onPress={() => this._navigateToChangeProfile()}>
                <Text style = {Styles.signInText}>Profile</Text>
              </TouchableOpacity>

              <TouchableOpacity style={Styles.buttonSignIn} onPress={() => this._logout()}>
                <Text style = {Styles.signInText}>Logout</Text>
              </TouchableOpacity>

            </Body>
          </CardItem>
        </Card>
        
      </Container>
      
    );
  }
}
