import { Platform, StyleSheet } from 'react-native';
import { Fonts, Metrics, Colors } from '../../Themes/';

const styles = StyleSheet.create({
  imgContainer: {
    width: Metrics.WIDTH,
  },
  sectionSeparator: {
    backgroundColor: Colors.sectionSeparator,
    marginTop: 16,
    // marginBottom: 16,
    height: 2
  },
  galleryList: {
    marginLeft: 16,
    marginRight: 16
  }
});

export default styles;