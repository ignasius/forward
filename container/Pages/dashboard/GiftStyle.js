import { Platform, StyleSheet } from 'react-native';
import { Fonts, Metrics, Colors } from '../../Themes/';

const styles = StyleSheet.create({
  sectionHeader: {
    color: Colors.sectionHeader,
    fontWeight: 'bold',
    fontSize: 24,
  },
  cardFooterContainer: {
    flex: 1, 
    flexDirection: 'column'
  },
  sectionSeparator: {
    backgroundColor: Colors.sectionSeparator,
    marginTop: 16,
    // marginBottom: 16,
    height: 2
  },
  sectionBlue: {
    color: Colors.blueFuchsia,
    fontSize: 14,
  },
});

export default styles;