
import React, { PropTypes, Component } from 'react';
import { 
  Platform, 
  StatusBar,
  Dimensions, 
  View, 
  Image, 
  ImageBackground, 
  Text, 
  TextInput, 
  TouchableOpacity, 
  BackHandler, 
  I18nManager
} from 'react-native';
import { 
  Container, 
  Content, 
  Header, 
  Body, 
  Title, 
  Button, 
  Icon, 
  Right, 
  Left, 
  Item, 
  Input, 
} from 'native-base';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import HTML from 'react-native-render-html';
import { Images } from '../../Themes';
// Screen Styles
import Styles from './styles';
import { RewardService } from '../../libs/api';

export default class RewardDetail extends Component {
  _id = -1;
  _title = "Reward Detail";
  _description = '';

  componentWillMount() {
    var that = this;

    BackHandler.addEventListener('hardwareBackPress', function() {
      that.navigateBack(false);
      
      return true;
    });
  }

  navigateBack = (submitted) => {
    if (submitted) {
      this.props.navigation.state.params.refresh();
    }

    this.props.navigation.goBack();
  }

  takeReward = () => {
    let that = this;

    RewardService
      .takeReward({ 
        rewardId: this._id,
      })
      .then((response) => {
        that.navigateBack(true);
      }).catch((error) => {
        let data = error.response.data;
        let message = data.msg ? data.msg : '';

        alert("Failure to take reward. " + message);
        console.log(error.response);
      });
  }

  render() {
    const imageUri = Images.background_main;

    StatusBar.setBarStyle("light-content", true);

    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("transparent", true);
      StatusBar.setTranslucent(true);
    }

    const { navigation } = this.props;
    let reward = navigation.getParam('reward', null);

    if (reward) {
      this._title = reward.title;
      this._id = reward.id;
      this._description = reward.description;
    }

    return (
      <Container>
        <ImageBackground style={Styles.imgContainer} source={imageUri}>
          <Header style={Styles.header}>
            <Left style={Styles.left}>
              <TouchableOpacity style={Styles.backArrow} onPress={() => this.navigateBack(false)}>
                <FontAwesome name={I18nManager.isRTL ? "angle-right" : "angle-left"} size={30} color="#fff"/>
              </TouchableOpacity>
            </Left>
            <Body style={Styles.body}>
              <Text style={Styles.titleNavigationBar}>{this._title}</Text>
            </Body>
            <Right style={Styles.right}/>
          </Header>
          <Content>
              <View style={Styles.actionDescription}>
                <Text style={Styles.title}>Congratulation !!</Text>
                <HTML html={this._description} />
              </View>

              <View style={Styles.actionContainer}>
                <TouchableOpacity style = {Styles.buttonRedeem} onPress = {() => this.takeReward()}>
                  <Text style = {Styles.signInText}>REDEEM CODE</Text>
                </TouchableOpacity>
              </View>
          </Content>
        </ImageBackground>
      </Container>
    );
  }
}
