import {
  Platform,
  StyleSheet,
  Dimensions
} from 'react-native';
import {
  Fonts,
  Metrics,
  Colors
} from '../../Themes/';

const styles = StyleSheet.create({
  sectionHeader: {
    color: Colors.sectionHeader,
    fontWeight: 'bold',
    fontSize: 24,
    marginTop: 16,
  },
  header: {
    backgroundColor: '#70DFE8',
    height: Metrics.HEIGHT * 0.1,
    borderBottomWidth: 0,
    elevation: 0,
    paddingLeft: (Metrics.WIDTH * 0.05),
    paddingBottom: 5,
  },
  left: {
    flex: 0.5,
    backgroundColor: 'transparent',
  },
  backArrow: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 30
  },
  body: {
    flex: 3,
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  titleNavigationBar: {
    color: Colors.white,
    fontSize: Fonts.moderateScale(16),
    marginTop: 5,
    alignSelf: 'center',
    fontFamily: Fonts.type.sfuiDisplaySemibold,
  },
  right: {
    flex: 0.5
  },
  imgContainer: {
    width: Metrics.WIDTH,
    height: Metrics.HEIGHT,
  },
  actionDescription: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flex: 1,
    flexDirection: 'column',
    marginRight: 16,
    marginLeft: 16,
    marginBottom: (Metrics.HEIGHT * 0.1),
  },
  title: {
    color: Colors.sectionHeader,
    fontWeight: 'bold',
    fontSize: 24,
    marginTop: (Metrics.HEIGHT * 0.05),
  },
  description: {
    fontSize: Fonts.moderateScale(14),
    fontWeight: '500',
    marginTop: (Metrics.HEIGHT * 0.05),
    fontFamily: Fonts.type.sfuiDisplaySemibold,
    lineHeight: 24,
  },
  actionContainer: {
    flex: 1,
    flexDirection: 'column',
    marginRight: 16,
    marginLeft: 16,
  },
  labelRedeem: {
    color: Colors.blueFuchsia,
    fontSize: 14,
  },
  textInput: {
    backgroundColor: "#fff",
    borderRadius: (Metrics.WIDTH * 0.42),
    borderColor: Colors.lightgrey,
    borderWidth: 2,
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
    alignSelf: 'stretch',
    flex: 1,
    fontSize: Fonts.moderateScale(16),
    fontFamily: Fonts.type.sfuiDisplayRegular,
  },
  buttonRedeem: {
    backgroundColor: "#70DFE8",
    borderRadius: 20,
    marginTop: 28,
    padding: 12,
    alignSelf: 'center',
    width: (Metrics.WIDTH * 0.5),
  },
  signInText: {
    textAlign: "center",
    fontSize: Fonts.moderateScale(16),
    fontWeight: 'bold',
    fontFamily: Fonts.type.sfuiDisplaySemibold,
    color: Colors.white,
  },
  galleryList: {
    marginLeft: 16,
    marginRight: 16,
  },
  sectionSeparator: {
    backgroundColor: Colors.sectionSeparator,
    marginTop: 16,
    height: 2
  },
  sectionBlue: {
    color: Colors.blueFuchsia,
    fontSize: 14,
  },
});
export default styles;