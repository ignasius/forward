import React, { Component } from 'react';
import { 
  Platform, 
  Dimensions, 
  StatusBar, 
  View, 
  Image,
  ImageBackground, 
  Text, 
  TouchableOpacity, 
  BackHandler, 
  I18nManager, 
} from 'react-native';
import { 
  Container,
  Header,
  Body,
  Right,
  Left,
} from 'native-base';
import Swiper from 'react-native-swiper';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import styles from './styles';
import { Images } from '../../Themes';
import { TokenStorage, ProfileStorage } from '../../libs/storage';
import SplashScreen from 'react-native-splash-screen'

export default class WalkthroughGetRide extends Component {
  _list = [{
      id: 1,
      image: Images.onboarding_1,
      top: {
        title: 'FILL IN THE FIELDS',
        style: styles.headertext
      },
      middle: {
        title: 'tap continue using this apps.',
        style: styles.desctext
      },
      bottom: {
        title: '',
        style: styles.desctext
      },
    }, {
      id: 2,
      image: Images.onboarding_2,
      top: {
        title: 'At the dashboard, you can',
        style: styles.desctext
      },
      middle: {
        title: 'CHECK 30 DAYS CHALLENGES',
        style: styles.headertext
      },
      bottom: {
        title: 'and upcoming events',
        style: styles.desctext
      },
    }, {
      id: 3,
      image: Images.onboarding_3,
      top: {
        title: 'You can check our latest events in',
        style: styles.desctext
      },
      middle: {
        title: 'gallery and also you can ',
        style: styles.desctext
      },
      bottom: {
        title: 'WATCH THE VIDEO',
        style: styles.headertext
      },
    }, {
      id: 4,
      image: Images.onboarding_4,
      top: {
        title: 'Enjoy your challenge and',
        style: styles.desctext
      },
      middle: {
        title: 'GOD BLESS YOU all the time!',
        style: styles.desctext
      },
      bottom: {
        title: '',
        style: styles.headertext
      },
    }
  ]

  constructor(props) {
     super(props);

     this.state = {};
  }

  componentWillMount() {
    let that = this;
    BackHandler.addEventListener('hardwareBackPress', function() {
      that.props.navigation.navigate('Walkthrough');
      return true;
    });
  }

  async componentDidMount() {
    let user = await ProfileStorage.getLocalProfile();
    let token = await TokenStorage.getToken();

    if (user && token) {
      this.props.navigation.navigate('Dashboard');
      
    }
    SplashScreen.hide();
    
  }
  
  render() {
    StatusBar.setBarStyle('dark-content', true);

    if (Platform.OS === 'android') {
      StatusBar.setBackgroundColor('rgba(0,0,0,0.3)',true);
      StatusBar.setTranslucent(true);
    }

    return(
      <Container style={styles.container}>
        <ImageBackground style={styles.backgroundImage} source={Images.onboarding_background} >
          <Header style={styles.header}>
            <Left style={styles.left}>
            </Left>
            <Body style={styles.body}>
            </Body>
            <Right style={styles.right}>
              <TouchableOpacity onPress={()=> this.props.navigation.navigate('Login')}>
                <Text style={styles.skipTxt}>Skip</Text>
              </TouchableOpacity>
            </Right>
          </Header>
  
          <View style={styles.slidesec}>
            <Swiper showsButtons={false}
              autoplay={true}
              autoplayTimeout={4.5}
              activeDot={<View style={styles.activeDot} />}
              dot={<View style={styles.dot} />}>
                {
                  this._list.map((item, index) => {
                    return (
                      <View style={styles.slide} key={index}>
                        <Image source={item.image} style={styles.sliderImage}/>
                        <View style={styles.contentStyle}>
                          <Text style={item.top.style}>
                            {item.top.title}
                          </Text>
                          <Text style={item.middle.style}>
                            {item.middle.title}
                          </Text>
                          <Text style={item.bottom.style}>
                            {item.bottom.title}
                          </Text>
                          {
                            item.id == 4 ? 
                              <TouchableOpacity style={styles.buttonSignIn} onPress={()=> this.props.navigation.navigate('Login')}>
                                <Text style = {styles.signInText}>GET STARTED</Text>
                              </TouchableOpacity>
                              : null
                          }
                        </View>
                      </View>
                    )
                  })
                }
            </Swiper>
          </View>
        </ImageBackground>
      </Container>
    );
  }
}
