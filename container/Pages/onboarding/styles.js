import { StyleSheet,Platform } from 'react-native';
import { Fonts, Metrics, Colors,Images } from '../../Themes';

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    width: Metrics.WIDTH,
    height: Metrics.HEIGHT,
    position: 'absolute'
  },
  container: {
    backgroundColor: Colors.white,
    // backgroundImage: Images.onboarding_background,
    width: (Metrics.WIDTH),
    height: (Metrics.HEIGHT)
  },
  backArrow:{
    width: 30,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  header: {
    backgroundColor: Colors.transparent,
    width: Metrics.WIDTH,
    flexDirection: 'row',
    borderBottomColor: Colors.transparent,
    height: (Metrics.HEIGHT * 0.1),
    elevation: 0,
    paddingTop: (Metrics.HEIGHT * 0.05),
    paddingLeft: (Metrics.WIDTH * 0.05),
    paddingRight: (Metrics.WIDTH * 0.05),
  },
  left: {
    flex: 0.5,
  },
  right: {
    flex: 0.5
  },
  body: {
    flex: 3,
    alignSelf: 'center'
  },
  skipTxt: {
    fontFamily: Fonts.type.sfuiDisplayMedium,
    fontSize: Fonts.moderateScale(16),
    color: '#ffffff',
    textAlign: 'right',
  },
  slidesec:{
    height: (Metrics.HEIGHT*0.92),
    backgroundColor: Colors.transparent
  },
  btnsec:{
    height: (Metrics.HEIGHT*0.12),
    justifyContent: 'center',
    backgroundColor: Colors.transparent
  },
  slide: {
    height: (Metrics.HEIGHT*0.78),
    backgroundColor: Colors.transparent
  },
  sliderImage: {
    resizeMode: 'contain',
    height: (Metrics.HEIGHT * 0.45),
    width: (Metrics.HEIGHT * 0.45),
    borderRadius: (Metrics.HEIGHT * 0.22),
    alignSelf: 'center',
    ...Platform.select({
      ios: {
        marginTop: (Metrics.HEIGHT * 0.04),
      },
      android: {
        marginTop: (Metrics.HEIGHT * 0.02),
      }
    }),
  },
  contentStyle: {
      marginTop: (Metrics.HEIGHT * 0.07),
  },
  headertext: {
    fontFamily: Fonts.type.sfuiDisplayLight,
    fontSize:Fonts.moderateScale(21),
    backgroundColor:Colors.transparent,
    color:'#ffffff',
    marginTop: (Metrics.HEIGHT * 0.012),
    textAlign:'center',
    alignSelf:'center',
    width :(Metrics.WIDTH) * 0.85,
  },
  desctext: {
    fontFamily: Fonts.type.sfuiDisplayRegular,
    fontSize:Fonts.moderateScale(16),
    backgroundColor:Colors.transparent,
    color:'#ffffff',
    marginTop: (Metrics.HEIGHT * 0.012),
    textAlign:'center',
    alignSelf:'center',
    width :(Metrics.WIDTH) * 0.7,
  },
  dot:{
    backgroundColor:'rgba(111,111,111,0.8)',
    width:  (Metrics.WIDTH * 0.02),
    height: (Metrics.WIDTH * 0.02),
    borderRadius: (Metrics.WIDTH * 0.01),
    marginTop: (Metrics.HEIGHT * 0.015),
    marginLeft: (Metrics.WIDTH * 0.030),
    marginRight: (Metrics.WIDTH * 0.030),
  },
  activeDot:{
    backgroundColor:'#fff',
    width:  (Metrics.WIDTH * 0.02),
    height: (Metrics.WIDTH * 0.02),
    borderRadius: (Metrics.WIDTH * 0.01),
    marginTop: (Metrics.HEIGHT * 0.015),
    marginLeft: (Metrics.WIDTH * 0.025),
    marginRight: (Metrics.WIDTH * 0.025),
  },
  discoverBg: {
    backgroundColor:'#0691ce',
    alignSelf: 'center',
    justifyContent: 'center',
    paddingLeft: (Metrics.WIDTH * 0.08),
    paddingRight: (Metrics.WIDTH * 0.08),
    paddingTop: (Metrics.WIDTH * 0.02),
    paddingBottom: (Metrics.WIDTH * 0.02),
    borderRadius: (Metrics.WIDTH * 0.06),
  },
  discoverTxt: {
    fontFamily: Fonts.type.sfuiDisplayMedium,
    fontSize: Fonts.moderateScale(12),
    color: Colors.white,
    textAlign: 'center'
  },
  buttonSignIn: {
    backgroundColor: "#70DFE8",
    borderRadius: 20,
    marginTop: 28,
    padding: 12,
    alignSelf: 'center',
    width: (Metrics.WIDTH * 0.4),
  },
  signInText: {
    color: Colors.white,
    textAlign: "center",
    fontSize: Fonts.moderateScale(16),
    fontFamily: Fonts.type.sfuiDisplaySemibold,
  },
});

export default styles;
