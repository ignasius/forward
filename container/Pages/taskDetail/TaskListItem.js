import React, { Component } from "react";
import { 
  View,
  Image,
  I18nManager 
} from 'react-native';
import { CheckBox, Card, CardItem, Text, Body } from "native-base";

class TaskListItem extends React.PureComponent {

  constructor(props) {
    super(props);

    this._populateDefaultState();
  }

  _populateDefaultState = async () => {
    this.state = {
      completed: this.props.item.completed
    }
  }

  _onPress = () => {
    this.props.onPressItem(this.props.item.id);
    this.setState({
      completed: this.props.item.completed
    });
  }

  render() {
    const item = this.props.item;

    return (
      <View style={{flex: 1, flexDirection: 'row', marginTop: 8, marginBottom: 8}}>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <Text>
            {item.task1}
          </Text>
        </View>
        <CheckBox style={{
          width: 20,
          marginRight: 16,
        }} checked={this.state.completed} onPress={this._onPress} />
      </View>
    );
  }
}

export default TaskListItem
