import React, { PropTypes, Component } from 'react';
import { Text, 
  View, 
  KeyboardAvoidingView,
  Platform, 
  StatusBar,
  Dimensions, 
  Image, 
  ImageBackground, 
  TextInput, 
  TouchableOpacity, 
  FlatList, 
  ListItem, 
  BackHandler, 
  I18nManager
} from 'react-native';
import { 
  Container, 
  Content, 
  Header, 
  Body, 
  Title, 
  Button, 
  Icon, 
  Right, 
  Left, 
  Item, 
  Input, 
  CheckBox
} from 'native-base';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Images, Colors } from '../../Themes';
import Styles from './styles';
import { ProfileService } from '../../libs/api';
import { ProfileStorage } from '../../libs/storage';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Toast from 'react-native-root-toast';

export default class ProfileDetail extends Component {
  constructor(props) {
    super(props);

    this._populateDefaultState();
    this._loadContent();
  }

  _populateDefaultState = async () => {
    this.state = {
      loading: false,
      refreshing: false,
      content: {
        email: '',
        name: '',
        phone: '',
        password: '',
        confirmPassword: '',
      }
    };
  }

  _loadContent = async () => {
    let user = await ProfileStorage.getLocalProfile();

    console.log(user);

    this.setState({
      loading: false,
      refreshing: false,
      content: {
        email: user.email,
        name: user.name,
        phone: user.phone,
        password: '',
        confirmPassword: '',
        isChangingPassword: () => {
          if (this.password || this.password.count > 0) {
            return true;
          }

          return false
        },
        isPasswordMatch: () => {
          if (this.password && this.confirmPassword) {
            return password == confirmPassword;
          }

          return false;
        }
      }
    });
  }

  componentWillMount() {
    let that = this;

    BackHandler.addEventListener('hardwareBackPress', function() {
      that.navigateBack();

      return true;
    });
  }

  navigateBack = () => {
    this.props.navigation.goBack();
  }

  updateProfile = async () => {
    let user = await ProfileStorage.getLocalProfile();
    const { name, phone, password, confirmPassword } = this.state.content;

    console.log(this.state.content);

    let isChangingPassword = false;
    
    if (this.state.content.password) {
      if (this.state.content.password.trim().length > 0) {
        isChangingPassword = true;
      }
    }

    let validConfirmationPassword = false;

    if (isChangingPassword) {
      if (this.state.content.password && this.state.content.confirmPassword) {
        validConfirmationPassword = this.state.content.confirmPassword.trim() == this.state.content.password.trim();
      }
    }

    if (isChangingPassword) {
      if (!validConfirmationPassword) {
        alert('Password is not match');

        return;
      }
    }

    ProfileService
      .updateProfile({ 
        name: name,
        phone: phone,
        password: (isChangingPassword && validConfirmationPassword) ? password : ''
      })
      .then((response) => {
        ProfileStorage.updateProfile(name, phone);
        let message= 'Successfully change profile';
        Toast.show(message, {
          duration: Toast.durations.LONG,
          position: Toast.positions.BOTTOM,
          shadow: true,
          animation: true,
          hideOnPress: true,
          delay: 0
      });
      }).catch((error) => {
        let data = error.response.data;
        let message = data.msg ? data.msg : '';

        alert("Failure to update name. " + message);
        console.log(error.response);
      });
  }

  updateName = (name) => {
    const contentCopy = Object.assign({}, this.state.content, { name });

    console.log('updating name: ' + name);
    this.setState({ content: contentCopy });
  }

  updatePhone = (phone) => {
    const contentCopy = Object.assign({}, this.state.content, { phone });

    console.log('updating phone: ' + phone);
    this.setState({ content: contentCopy });
  }

  updatePassword = (password) => {
    const contentCopy = Object.assign({}, this.state.content, { password });

    console.log('updating password: ' + password);
    this.setState({ content: contentCopy });
  }

  updateConfirmPassword = (confirmPassword) => {
    const contentCopy = Object.assign({}, this.state.content, { confirmPassword });

    console.log('updating confirmPassword: ' + confirmPassword);
    this.setState({ content: contentCopy });
  }

  render() {
    const imageUri = Images.background_main;

    StatusBar.setBarStyle("light-content", true);

    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("transparent", true);
      StatusBar.setTranslucent(true);
    }
    
    return (
      
      <Container>
        <ImageBackground style={Styles.imgContainer} source={imageUri}>
          <Header style={Styles.header}>
            <Left style={Styles.left}>
              <TouchableOpacity style={Styles.backArrow} onPress={() => this.navigateBack()}>
                <FontAwesome name={I18nManager.isRTL ? "angle-right" : "angle-left"} size={30} color="#fff"/>
              </TouchableOpacity>
            </Left>
            <Body style={Styles.body}>
              <Text style={Styles.titleNavigationBar}>Edit Profile</Text>
            </Body>
            <Right style={Styles.right}/>
          </Header>
          <Content>
            <KeyboardAwareScrollView
              style={Styles.actionContainer}
              enableOnAndroid={true} 
              behavior="padding">
              <Text style={Styles.sectionHeader}>Edit Profile</Text>
              
              <View style={Styles.sectionSeparator}>
              </View>

              <Text style={Styles.sectionHeader}>Email</Text>
              <TextInput
                style={{
                  marginTop: 16,
                  borderColor: Colors.whitee,
                  borderWidth: 1,
                  padding: 8,
                }}
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                textAlign={I18nManager.isRTL ? 'right' : 'left'}
                value={this.state.content.email}
                editable={false} />

              <Text style={Styles.sectionHeader}>Full Name</Text>
              <TextInput
                style={{
                  marginTop: 16,
                  borderColor: Colors.whitee,
                  borderWidth: 1,
                  padding: 8,
                }}
                placeholder="Please provide your name"
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                textAlign={I18nManager.isRTL ? 'right' : 'left'}
                keyboardType="default" 
                value={this.state.content.name}
                onChangeText={(name) => this.updateName(name)} />

              <Text style={Styles.sectionHeader}>Phone</Text>
              <TextInput
                style={{
                  marginTop: 16,
                  borderColor: Colors.whitee,
                  borderWidth: 1,
                  padding: 8,
                }}
                placeholder="Please provide your phone"
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                textAlign={I18nManager.isRTL ? 'right' : 'left'}
                keyboardType="phone-pad" 
                value={this.state.content.phone}
                onChangeText={(phone) => this.updatePhone(phone)} />

              <Text style={Styles.sectionHeader}>Password *</Text>
              <TextInput secureTextEntry={true} 
                style={{
                  marginTop: 16,
                  borderColor: Colors.whitee,
                  borderWidth: 1,
                  padding: 8,
                }}
                placeholder="Please provide your password, if you want to update password"
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                textAlign={I18nManager.isRTL ? 'right' : 'left'}
                keyboardType="default" 
                value={this.state.content.password}
                onChangeText={(password) => this.updatePassword(password)} />

              <Text style={Styles.sectionHeader}>Confirm Password</Text>
              <TextInput secureTextEntry={true} 
                style={{
                  marginTop: 16,
                  borderColor: Colors.whitee,
                  borderWidth: 1,
                  padding: 8,
                }}
                placeholder="Please put confirmation password"
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                textAlign={I18nManager.isRTL ? 'right' : 'left'}
                keyboardType="default" 
                value={this.state.content.confirmPassword}
                onChangeText={(confirmPassword) => this.updateConfirmPassword(confirmPassword)} />

              <TouchableOpacity style={Styles.buttonRedeem} onPress={() => this.updateProfile()}>
                <Text style={Styles.signInText}>SUBMIT</Text>
              </TouchableOpacity>
            </KeyboardAwareScrollView>
          </Content>
        </ImageBackground>
      </Container>
    );
  }
}
