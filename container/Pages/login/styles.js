
import { Platform, StyleSheet, Dimensions } from 'react-native';
import { Fonts, Metrics, Colors } from '../../Themes/';

const styles = StyleSheet.create({
  header: {
    backgroundColor: Colors.transparent,
    height: Metrics.WIDTH * 0.15,
    borderBottomWidth: 0,
    ...Platform.select({
      ios: {},
      android: {
				marginTop: Fonts.moderateScale(15)
			}
    }),
		elevation: 0
  },
	left: {
		flex: 0.5,
		backgroundColor: 'transparent',
  },
	backArrow: {
		justifyContent: 'center',
		alignItems: 'center',
    width: 30
  },
	body: {
		flex: 3,
		alignItems: 'center',
		backgroundColor: 'transparent'
  },
	textTitle: {
    color: Colors.white,
    fontSize: Fonts.moderateScale(16),
    marginTop: 5,
    alignSelf: 'center',
	  fontFamily: Fonts.type.sfuiDisplaySemibold,
  },
  welcome:{
    color: Colors.white,
    fontSize: Fonts.moderateScale(30),
    fontWeight:'700',
    marginTop: (Metrics.HEIGHT* 0.05),
    marginBottom: (Metrics.HEIGHT* 0.02),
    alignSelf: 'center',
	  fontFamily: Fonts.type.sfuiDisplaySemibold,
  },
  description: {
      width: (Metrics.WIDTH * 0.7),
    color: Colors.white,
    textAlign: 'center',
    fontSize: Fonts.moderateScale(16),
    fontWeight:'500',
    marginTop: (Metrics.HEIGHT* 0.05),
    marginBottom: (Metrics.HEIGHT* 0.02),
    alignSelf: 'center',
	  fontFamily: Fonts.type.sfuiDisplaySemibold,
  },
	right: {
    flex: 0.5
  },
  imgContainer: {
    width: Metrics.WIDTH,
    height: Metrics.HEIGHT,
  },
  logoSec:{
    width: Metrics.WIDTH,
    height: Metrics.HEIGHT * 0.24,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: (Metrics.HEIGHT * 0.1),
  },
  textInput: {
    backgroundColor: "#fff",
    borderRadius: (Metrics.WIDTH * 0.42),
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
    alignSelf: 'center',
    width: (Metrics.WIDTH * 0.84),
    fontSize: Fonts.moderateScale(16),
    fontFamily: Fonts.type.sfuiDisplayRegular,
  },
  buttonSignIn: {
    backgroundColor: "#70DFE8",
    borderRadius: 20,
    marginTop: 28,
    padding: 12,
    alignSelf: 'center',
    width: (Metrics.WIDTH * 0.4),
  },
  signInText: {
    color: Colors.white,
    textAlign: "center",
    fontSize: Fonts.moderateScale(16),
    fontFamily: Fonts.type.sfuiDisplaySemibold,
  },
  registerText: {
    textAlign: 'center',
    fontSize: Fonts.moderateScale(14),
    color: Colors.white,
    marginTop: 30,
    marginBottom: 30,
    fontFamily: Fonts.type.sfuiDisplayRegular,
  },
});
export default styles;
