import React, { Component } from "react";
import { 
  View, 
  I18nManager 
} from 'react-native';
import { Card, CardItem, Text, Body } from "native-base";
import MonthView from "./MonthView"
import Styles from '../dashboard/HomeStyle';

class CalendarView extends React.PureComponent {
  render() {
    const item = this.props.item;

    return (
      <Card transparent>
        <CardItem header>
          <Text style={Styles.sectionBlue}>{item.title}</Text>
        </CardItem>
        <CardItem>
          <Body>
              <MonthView 
                navigation={this.props.navigation} 
                totalDayCompleted={item.totalDayCompleted} 
                readOnly={true} 
                navigateToForward={this.props.navigateToForward} />
          </Body>
        </CardItem>
        <CardItem footer>
          <View style={Styles.cardFooterContainer}>
            <View style={Styles.sectionSeparator}>
            </View>
          </View>
        </CardItem>
      </Card>
    );
  }
}

export default CalendarView
