import React, { Component } from "react";
import { 
  View, 
  ListView,
  I18nManager 
} from 'react-native';
import { Card, CardItem, Text, Body } from "native-base";
import { Images, Fonts, Metrics, Colors } from "../../Themes/";
import Styles from '../dashboard/HomeStyle';
import EventListItem from "../home/EventListItem"

class EventView extends React.PureComponent {
  _ds = null;

  constructor(props) {
    super(props);

    this._ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: this._ds.cloneWithRows(this.props.item.data),
    };
  }

  _renderRow = (item) => {
    return (
      <EventListItem
        item={item}
        onPressItem={this._onEventSelected}
      />
    )
  }

  _onEventSelected = (index) => {
    console.log("Pressed row: " + index);
    this.props.navigation.navigate('EventDetail', this.props.item.data[index-1]);
  };

  render() {
    const item = this.props.item;
    let newContent = this._ds.cloneWithRows(this.props.item.data);

    return (
      <Card transparent>
        <CardItem header>
          <Text style={Styles.sectionBlue}>{item.title}</Text>
        </CardItem>
        <CardItem>
          <Body>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <ListView
                dataSource={newContent}
                extraData={this.state.dataSource}
                renderRow={this._renderRow} />
            </View>
          </Body>
        </CardItem>
        <CardItem footer>
          <View style={Styles.cardFooterContainer}>
            <View style={Styles.sectionSeparator}>
            </View>
          </View>
        </CardItem>
      </Card>
    );
  }
}

export default EventView
