import React, { Component } from "react";
import { 
  View, 
  I18nManager 
} from 'react-native';
import { Card, CardItem, Text, Body } from "native-base";
import Styles from '../dashboard/HomeStyle';
import HTML from 'react-native-render-html';



class GreetingView extends React.PureComponent {
  render() {
    const item = this.props.item;

    return (
      <Card transparent>
        <CardItem header>
          <Text style={Styles.sectionHeader}>{item.title}</Text>
        </CardItem>
        <CardItem>
          <Body>
            <View>
              {/* <Text> */}
              <HTML html={item.description} />
                {/* {result} */}
              {/* </Text> */}
            </View>
          </Body>
        </CardItem>
        <CardItem footer>
          <View style={Styles.cardFooterContainer}>
            <Text>
              {item.quote}
            </Text>
            <View style={Styles.sectionSeparator}>
            </View>
          </View>
        </CardItem>
      </Card>
    );
  }
}

export default GreetingView
