/* eslint global-require: 0 */
const images = {
  splash: require('../Images/splash.png'),
  onboarding_background: require('../Images/onboarding_background.png'),
  onboarding_1: require('../Images/onboarding_1.png'),
  onboarding_2: require('../Images/onboarding_2.png'),
  onboarding_3: require('../Images/onboarding_3.png'),
  onboarding_4: require('../Images/onboarding_4.png'),
  login_background: require('../Images/login_background.png'),
  background_main: require('../Images/background_main.png'),
  sheep: require('../Images/sheep.png'),
  tab_home_active: require('../Images/tab_home_active.png'),
  tab_challenge_active: require('../Images/tab_challenge_active.png'),
  tab_gallery_active: require('../Images/tab_gallery_active.png'),
  tab_reward_active: require('../Images/tab_reward_active.png'),
  sponsors: require('../Images/sponsors.png'),
};

export default images;
