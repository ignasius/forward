const colors = {
  // app color
  background: '#1F0808',
  transparent: 'rgba(0, 0, 0, 0)',

  error: 'rgba(200, 0, 0, 0.8)',
  border: '#483F53',
  text: '#E0D7E5',

  black: 'black',
  red: 'red',
  ember: 'rgba(164, 0, 48, 0.5)',

  blue: 'blue', 
  loginBlue: '#118BDC',
  blueText: '#148EDD',
  turquoiseBlue: '#75DFE7',
  blueFuchsia: '#674FB7',
  babyBlueEyes: '#A0C7EF',

  green: 'green',
  
  white: 'white',
  whitee: '#eeeeee',
  snow: 'white',
  ricePaper: 'rgba(255, 255, 255, 0.75)',
  
  sectionHeader: '#686D7A',
  sectionSeparator: '#F1F1F2',
  
  gray: '#d8d8d8',
  lightgrey: '#e2e2e2',
  coal: '#2d2d2d',
  
  purpleHeart: '#5A3BB4',
  
  calendarCellBlur: 'rgba(255,255,255,0.75)',
  calendarCellBorder: '#E2C4F1',

  // Blue 0821 C
  forwardLightBlue:'#70dfe8',
  // Blue 2131 C
  forwardDarkBlue:'#2a56c8',
  // Purple 266 C
  forwardPurple:'#5a3bb4',
  // Pink C
  forwardPink:'#ed2493'
};

export default colors;
