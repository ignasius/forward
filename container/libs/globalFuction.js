const globalFuction = {
  getMonthDesc(month) {
    dataDesc = ['Januari', 'Febuari', 'Maret', 'April', 'Mai', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    return dataDesc[month - 1];
  },
  convertFormatddMMyyyy(pickedValue) {
    let dd = pickedValue[2];
    let MM = pickedValue[1];
    let yyyy = pickedValue[0];
    return `${dd}-${MM}-${yyyy}`;
  },
  parseNumFormat(amount) {
    return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  },
  parseNumValue(amount) {
    return amount.replace(/,/g, '');
  },
  roundUpAmount(amount) {
    // return this.parseNumFormat(Math.round(Number(this.parseNumValue(amount))/100000)*100000);
    let result = 0;
    if (amount.toString().length < 6) {
      result = 100000;
    } else {
      result = amount;
    }
    return Math.round(result / 100000) * 100000;
  },
  getRateColor(rate) {
    let hexCode = '';
    if (rate == 1) {
      hexCode = '#7EBB45';
    } else if (rate == 2) {
      hexCode = '#377FC7';
    } else if (rate == 3) {
      hexCode = '#FF9927';
    } else if (rate == 4) {
      hexCode = '#EC3E40';
    }
    return hexCode;
  }

};

export default globalFuction;