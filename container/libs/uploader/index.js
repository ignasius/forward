import axios from 'axios';
import {
  AsyncStorage
} from 'react-native';
// import Config from 'react-native-config';
import * as Upload from './upload';

axios.defaults.baseURL = 'https://api.lojconference.id';
axios.defaults.headers.common['Content-Type'] = 'multipart/form-data';

axios.interceptors.request.use(async (config) => {
  const value = await AsyncStorage.getItem('JWT_BEARER_TOKEN');

  if (value !== null) {
    axios.defaults.headers.common['Authorization'] = value;
  }

  return config;
}, error => Promise.reject(error));

export {
  Upload,
};