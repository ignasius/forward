import axios from 'axios';
import API from '../../API';

const uploadKtp = function (data) {

  const formData = new FormData();
  formData.append('id', data.id);
  formData.append('ktp', data.ktp);
  formData.append('ktpImage', data.ktpImage);

  return axios.post(API.ktpRegister, formData)
    .then(response => response.data)
    .catch((error) => {
      alert(JSON.stringify(error.response));
    });
};

const uploadNpwp = function (data) {
  const formData = new FormData();
  formData.append('id', data.id);
  formData.append('npwp', data.npwp);
  formData.append('npwpImage', data.npwpImage);

  return axios.post(API.npwpRegister, formData)
    .then(response => response.data)
    .catch((error) => {
      alert(JSON.stringify(error.response));
    });
};

const skipNpwp = function (data) {
  const formData = new FormData();
  formData.append('id', data.id);
  return axios.post(API.npwpSkip, formData)
    .then(response => response.data)
    .catch((error) => {
      alert(JSON.stringify(error.response));
    });
};

export {
  uploadKtp,
  uploadNpwp,
  skipNpwp,
};