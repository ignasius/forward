import {
  AsyncStorage
} from 'react-native';
import * as ProfileStorage from './profileStorage';
import * as TokenStorage from './tokenStorage';

export {
  ProfileStorage,
  TokenStorage,
};