import {
  AsyncStorage
} from 'react-native'

const setLocalProfile = async (value) => {
  return await AsyncStorage.setItem('USER', JSON.stringify(value));
};

const getLocalProfile = async () => {
  let content = await AsyncStorage.getItem('USER');
  let profile = await JSON.parse(content);

  return profile;
};

const updateProfile = async (name, phone) => {
  let content = await AsyncStorage.getItem('USER');
  let profile = await JSON.parse(content);

  profile.name = name;
  profile.phone = phone;

  await AsyncStorage.setItem('USER', JSON.stringify(profile));
};

const removeProfile = async () => {
  try {
    await AsyncStorage.removeItem('USER');
    return true;
  } catch (exception) {
    return false;
  }
};

export {
  setLocalProfile,
  getLocalProfile,
  removeProfile,
  updateProfile,
};