import {
  AsyncStorage
} from 'react-native'

const setToken = async (value) => {
  return await AsyncStorage.setItem('JWT_BEARER_TOKEN', `Bearer ${value}`);
};

const getToken = async () => {
  return await AsyncStorage.getItem('JWT_BEARER_TOKEN');
};

const removeToken = async () => {
  try {
    await AsyncStorage.removeItem('JWT_BEARER_TOKEN');
    return true;
  } catch (exception) {
    return false;
  }
};

export {
  setToken,
  getToken,
  removeToken,
};