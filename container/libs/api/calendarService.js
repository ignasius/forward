import axios from 'axios';
import API from '../../API';

const getCalendar = function () {
  return axios.get(API.calendar)
    .then((response) => {
      const responseData = response.data;

      return responseData;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

const getCalendarDetail = function (day) {
  return axios.get(API.calendar + '/' + day)
    .then((response) => {
      const responseData = response.data;

      return responseData;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

const submitProgress = function (progress) {
  return axios.post(API.submitProgress, progress)
    .then(response => response.data)
    .catch((error) => {
      return Promise.reject(error);
    });
};

export {
  getCalendar,
  getCalendarDetail,
  submitProgress
};