import axios from 'axios';
import API from '../../API';

const getGalleries = function () {
  return axios.get(API.galleries)
    .then((response) => {
      const responseData = response.data;

      return responseData;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

const getGalleryDetail = function (id) {
  return axios.get(API.galleries + '/' + id)
    .then((response) => {
      const responseData = response.data;

      return responseData;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

export {
  getGalleries,
  getGalleryDetail
};