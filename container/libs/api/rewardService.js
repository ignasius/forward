import axios from 'axios';
import API from '../../API';

const getRewards = function () {
  return axios.get(API.rewards)
    .then((response) => {
      const responseData = response.data;

      return responseData;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

const takeReward = function (data) {
  return axios.post(API.submitReward, data)
    .then((response) => {
      const responseData = response.data;

      return responseData;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

export {
  getRewards,
  takeReward,
};