import axios from 'axios';
import API from '../../API';

const getQuotes = function () {
  return axios.get(API.quotes)
    .then((response) => {
      const responseData = response.data;

      return responseData;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

export {
  getQuotes,
};