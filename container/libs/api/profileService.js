import axios from 'axios';
import API from '../../API';

const updateProfile = function (data) {
  return axios.put(API.updateProfile, data)
    .then((response) => {
      const responseData = response.data;

      return responseData;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

const updateName = function (data) {
  return axios.put(API.updateName, data)
    .then((response) => {
      const responseData = response.data;

      return responseData;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

const updatePhone = function (data) {
  return axios.put(API.updatePhone, data)
    .then((response) => {
      const responseData = response.data;

      return responseData;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};

export {
  updateProfile,
  updateName,
  updatePhone,
};