import axios from 'axios';
import {
  AsyncStorage
} from 'react-native';
// import Config from 'react-native-config';
import * as AuthService from './authService';
import * as DashboardService from './dashboardService';
import * as EventService from './eventService';
import * as CalendarService from './calendarService';
import * as QuoteService from './quoteService';
import * as MessageService from './messageService';
import * as GalleryService from './galleryService';
import * as ProfileService from './profileService';
import * as RewardService from './rewardService';
import * as TokenStorage from '../storage/tokenStorage';

axios.defaults.baseURL = 'https://api.lojconference.id';

axios.interceptors.request.use(async (config) => {
  const value = await TokenStorage.getToken();

  if (value !== null) {
    config.headers.authorization = value;
  }

  return config;
}, error => Promise.reject(error));

export {
  AuthService,
  DashboardService,
  EventService,
  CalendarService,
  QuoteService,
  MessageService,
  GalleryService,
  ProfileService,
  RewardService,
};