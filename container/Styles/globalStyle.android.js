import { Platform, StyleSheet } from 'react-native';
// Screen Styles
import { Fonts, Metrics, Colors } from '../Themes';

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    width: Metrics.WIDTH,
    height: Metrics.HEIGHT,
  },
  container: {
    backgroundColor: 'transparent',
  },
  logostyle: {
    alignSelf: 'center',
    width: Metrics.WIDTH * 0.57,
    height: Metrics.HEIGHT * 0.083,
    backgroundColor: 'transparent',
    resizeMode: 'contain',
  },
  backArrow: {
    marginTop: 10,
    width: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backArrow02: {
    width: 30,
    alignItems: 'center',
  },
  textTitle: {
    color: 'white',
    fontSize: Fonts.moderateScale(16),
    alignSelf: 'center',
    textAlign: 'center',
  },
  headTitle: {
    borderBottomWidth: 0,
    marginTop: Metrics.HEIGHT * 0.025,
  },
  headTitleFont: {
    color: Colors.blueText,
    fontWeight: '700',
  },
  header: {
    backgroundColor: Colors.transparent,
    height: Metrics.WIDTH * 0.1,
    borderBottomWidth: 0,
    marginTop: Fonts.moderateScale(20),
    elevation: 0,
  },
  header02: {
    backgroundColor: Colors.loginBlue,
    height: Metrics.WIDTH * 0.18,
    borderBottomWidth: 0,
    paddingTop: Fonts.moderateScale(20),
    elevation: 0,
    alignItems: 'center',
  },
  headerImage02: {
    height: Metrics.WIDTH * 0.15,
    width: Metrics.WIDTH,
    position :'absolute',
    top: 20,
  },
  left: {
    flex: 0.5,
    backgroundColor: 'transparent',
  },
  right: {
    flex: 0.5,
  },
  inputStyle: {
    borderColor: 'transparent',
    justifyContent: 'center',
    alignSelf: 'center',
    width: Metrics.WIDTH * 0.8,
    margin: 5,
  },
  defaultBottombtn: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingTop: 5,
    paddingBottom: 5,
    borderRadius: 40,
    borderWidth: 1.5,
    borderColor: 'white',
    width: Metrics.WIDTH * 0.8,
    height: Metrics.HEIGHT * 0.06,
    position: 'absolute',
    bottom: Metrics.HEIGHT * 0.06,
  },
  buttongettextRight: {
    fontFamily: Fonts.type.sfuiDisplaySemibold,
    color: Colors.white,
    alignSelf: 'flex-end',
    marginTop: Metrics.HEIGHT * 0.0001,
    marginRight: Metrics.WIDTH * 0.1,
  },
  bottomPart: {
    position: 'absolute',
    alignSelf: 'center',
    justifyContent: 'center',
    bottom: Metrics.HEIGHT * 0.1,
  },
  buttongetstarted: {
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: Fonts.type.sfuiDisplaySemibold,
    color: Colors.white,
  },
  centerText: {
    width: '100%',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  bottomText01: {
    fontSize: Fonts.moderateScale(16),
    color: '#FFFFFF',
    fontFamily: Fonts.type.sfuiDisplayRegular,
  },
  bottomText02: {
    fontSize: Fonts.moderateScale(12),
    fontFamily: Fonts.type.sfuiDisplayRegular,
    color: '#FFFFFF',
  },
  bottomText03: {
    fontSize: Fonts.moderateScale(10),
    fontFamily: Fonts.type.sfuiDisplayRegular,
    color: '#FFFFFF',
  },
  bottomText04: {
    fontSize: Fonts.moderateScale(14),
    fontFamily: Fonts.type.sfuiDisplayRegular,
    color: '#FFFFFF',
  },
});
export default styles;
