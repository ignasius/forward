// collections for endpoints
class API {
  // Public Routes
  static baseURL = 'https://api.lojconference.id'

  // auth
  static register = '/public/register'
  static login = '/public/login'

  // dashboard
  static dashboard = '/private/dashboard'

  // event
  static event = '/public/events'

  // calendar
  static calendar = '/private/calendars'
  static submitProgress = '/private/submit/calendar'

  // gallery
  static galleries = '/public/galleries'

  // profile
  static updateProfile = 'private/users/profile'
  static updateName = '/private/users/profile/name'
  static updatePhone = '/private/users/profile/phone'

  // reward
  static rewards = 'private/redeem'
  static submitReward = 'private/submit/reward'

  // quote
  static quotes = 'public/quotes'

  // quote
  static allTaskCompletedMessage = 'private/complete'
}

export default API;